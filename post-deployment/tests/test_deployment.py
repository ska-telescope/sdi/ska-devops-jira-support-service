# import json
# import logging

import requests
import json
import logging
import pytest

def test_get():
    url = 'http://test-jira-service-ska-devops-jira-support-service:80/'

    response = requests.get(url)
    assert response.status_code == 200

@pytest.mark.skip(reason="no way of currently testing this")
def test_post_events():
    url = 'http://test-jira-service-ska-devops-jira-support-service:80/jira/'

    with open("resources/event.json", "r") as myfile:
            data = myfile.read()

    obj = json.loads(data)

    response = requests.post(url, json=obj)
    assert response.status_code == 200

