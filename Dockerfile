FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

RUN pip install poetry
COPY poetry.lock pyproject.toml ./
RUN poetry config virtualenvs.create false && poetry install --no-dev --no-root

COPY ./app /app
ENV PYTHONPATH=${PYTHONPATH}:../