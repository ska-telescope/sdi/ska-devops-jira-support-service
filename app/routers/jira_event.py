""" JIRA Support ticket router """
import logging
import os

import requests
from fastapi import APIRouter, Form, Response, status

from app.models.create_issue_action import CreateSupportIssueAction
from app.models.create_message_preview import CreateSupportIssueMessagePreview
from app.models.user_authorization import authorise_user

# from fastapi.responses import JSONResponse


logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)
# logger = logging.getLogger("uvicorn.error")

router = APIRouter()

SLACK_BOT_TOKEN = os.environ.get("SLACK_BOT_TOKEN")


@router.get("/")
async def return_teacup():
    """ Return all events """
    return {418: {"Description": "I'm a teapot"}}


@router.post("/message_shortcuts")
async def new_support_ticket_from_message_shortcut(payload: str = Form(...)):

    import json

    data = json.loads(payload)
    logger.info(data)

    if (
        data["type"] == "message_action"
        and data["callback_id"] == "create_support_issue"
    ):
        # Create Message Preview result
        message_obj = data["message"]
        description = message_obj["text"]
        summary = message_obj["text"][:50] + "..."
        message_preview = CreateSupportIssueMessagePreview()
        trigger_id = data["trigger_id"]
        user_id = data["user"]["id"]
        logger.info("Slack Username of the triggerer: %s", user_id)
        if not authorise_user(user_id):
            response = requests.post(
                url=data["response_url"],
                json={
                    "text": "This user is not authorised to use this shortcut! Only "
                    "System Team Members can use it. If you are part of the "
                    "system team, contact #team-system-support channel",
                    "response_type": "ephemeral",
                },
                headers={"Authorization": f"Bearer {SLACK_BOT_TOKEN}"},
            )
            return Response(status_code=status.HTTP_200_OK)
        response_url = "https://slack.com/api/views.open"

        preview = message_preview.preview(
            trigger_id=trigger_id,
            description=description,
            summary=summary,
            user_id=user_id,
        )
        logger.info("Request: %s", preview)
        # response_url = "https://ptsv2.com/t/hknb4-1605701563/post" # Debug
        logger.info("Response URL: %s", response_url)
        # preview = {"text": "Test text"}
        response = requests.post(
            url=response_url,
            json=preview,
            headers={"Authorization": f"Bearer {SLACK_BOT_TOKEN}"},
        )
        logger.info("Reponse from SLACK: %s", response.text)
        return Response(status_code=status.HTTP_200_OK)

    elif (
        data["type"] == "view_submission"
        and data["view"]["callback_id"] == "view_create_support_issue"
    ):
        import pprint as pp

        pp.pprint(data)
        submitted_data = data["view"]["state"]["values"]
        triggering_user = data["user"]
        summary = submitted_data["block-issue-summary"]["action-issue-summary"]["value"]
        description = submitted_data["block-issue-description"][
            "action-issue-description"
        ]["value"]
        selected_user = submitted_data["block-issue-assignee"]["action-issue-assignee"][
            "selected_option"
        ]
        jira_username = selected_user["value"]
        logger.info("Summary: %s", summary)
        logger.info("Description: %s", description)
        logger.info("Jira Username: %s", jira_username)
        logger.info("Slack User Info: %s", triggering_user)

        # THIS IS THE RESULT THAT WILL BE RETURNED IF NOTHING GOES WRONG
        # result = JSONResponse(
        #     status_code=status.HTTP_200_OK, content={"response_action": "clear"}
        result = Response(status_code=status.HTTP_200_OK)
        # result = {"response_action": "clear"}

        if not authorise_user(triggering_user["id"]):
            response = requests.post(
                url=data["response_url"],
                json={
                    "text": "You have accessed a window you shouldn't be able to. "
                    "Please ask for access in the #team-system-support channel.",
                    "response_type": "ephemeral",
                },
                headers={"Authorization": f"Bearer {SLACK_BOT_TOKEN}"},
            )
            return Response(status_code=status.HTTP_200_OK)

        # Create Jira ticket
        logger.info("Creating JIRA Issue...")
        action_executor = CreateSupportIssueAction()
        # jira_result = await action_executor.action(
        #     issue_summary=summary,
        #     issue_description=description,
        #     assignee_username=jira_username,
        # )
        # BELOW IS FOR TESTING AGAINST THE CHR PROJECT
        jira_result = await action_executor.action(
            proj_id="CHR",
            issue_type="Chairs",
            issue_summary=summary,
            issue_description=description,
            assignee_username=jira_username,
        )
        logger.info("Jira returned: %s", jira_result)
        # if jira returned an error, modify the result
        error = jira_result[0]["error"]
        if error:
            result = error
            logger.error(result)
        else:
            issue_key = jira_result[0]["issue"].key
            logger.info("Issue created: %s", issue_key)

        # logger.info("Returning (not final response): %s", result)
        return Response(status_code=status.HTTP_200_OK)
        # return result

    # result = Response(status_code=status.HTTP_200_OK)
    # logger.info("Returning: %s", result)
    return Response(status_code=status.HTTP_200_OK)
    # return result


@router.post("/slash_commands/support")
async def new_support_ticket_slash_command(
    user_id: str = Form(...),
    text: str = Form(...),
    trigger_id: str = Form(...),
    response_url: str = Form(...),
):

    # Create Message Preview result
    description = text
    summary = text[:50] + "..."
    message_preview = CreateSupportIssueMessagePreview()
    jira_username = authorise_user(user_id)
    logger.info("Jira Username of the triggerer: %s", jira_username)
    if jira_username is None:
        response = requests.post(
            url=response_url,
            json={
                "text": "This user is not authorised to use this shortcut!"
                "Only System Team Members can use it."
                "If you are part of the system team,"
                "contact #team-system-support channel",
                "response_type": "ephemeral",
            },
            headers={"Authorization": f"Bearer {SLACK_BOT_TOKEN}"},
        )
        return Response(status_code=status.HTTP_200_OK)
    response_url = "https://slack.com/api/views.open"

    preview = message_preview.preview(
        trigger_id=trigger_id,
        description=description,
        summary=summary,
        user_id=user_id,
    )
    logger.info("Request: %s", preview)
    # response_url = "https://ptsv2.com/t/hknb4-1605701563/post" # Debug
    logger.info("Response URL: %s", response_url)
    # preview = {"text": "Test text"}
    response = requests.post(
        url=response_url,
        json=preview,
        headers={"Authorization": f"Bearer {SLACK_BOT_TOKEN}"},
    )
    logger.info("Reponse from SLACK: %s", response.text)
    return Response(status_code=status.HTTP_200_OK)
