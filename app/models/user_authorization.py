# Jira username and Slack Id list
system_team_users = {
    "b.morgado": "UCXPF6M7A",
    "a.debeer": "UL1UM7UP6",
    "b.riberio": "U01CR4SCE48",
    "d.maia": "UUP9CS807",
    "d.regateiro": "U01C19DCV50",
    "d.gautam": "U019ELX89TP",
    "d.nunes": "UMSJT1XJ5",
    "d-carlo.matteo": "U1TB6NZ0Q",
    "p.harding": "U9GGM46TB",
    "u.yilmaz": "UUEKTT10A",
}


def authorise_user(id: str = None):
    """Authorise users for functionality on Slack app

    :param id: Slack ID of user to be authenticated
    :return: None
    """

    for jira_username, slack_id in system_team_users.items():
        if id == slack_id:
            return jira_username
    return None
