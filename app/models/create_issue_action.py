from ska import jira_api


class CreateSupportIssueAction:
    def __init__(self):
        self.jira = jira_api.SKAJira()

    async def action(
        self,
        proj_id: str = "ST",
        issue_summary=None,
        issue_description=None,
        issue_type: str = "Enabler",
        assignee_username: str = None,
    ):
        await self.jira.authenticate()
        issue_dict = {
            "project": {"key": proj_id},
            "summary": issue_summary,
            "description": issue_description,
            "issuetype": {"name": issue_type},
            "assignee": {"name": assignee_username},
        }
        result = await self.jira.create_issues([issue_dict])
        return result
