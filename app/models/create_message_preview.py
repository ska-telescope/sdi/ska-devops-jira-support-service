from ska import jira_api

user_list = {
    "BUTTONS | Klaassen, Pamela": "p.klaassen",
    "CIPA | Pleasance, Michael": "m.pleasance",
    "CREAM | Carlo Baffa": "c.baffa",
    "Karoo | Swart, Paul": "p.swart",
    "MCCS | Waterson, Mark": "m.waterson",
    "NCRA | Vivek Mohile": "v.mohile",
    "Perentie | Hampson, Grant": "g.hampson",
    "VIOLA | Hayden, Daniel": "d.hayden",
    "SYSTEM | <@UCXPF6M7A>": "b.morgado",
    "SYSTEM | <@UL1UM7UP6>": "a.debeer",
    "SYSTEM | <@U01CR4SCE48>": "b.riberio",
    "SYSTEM | <@UUP9CS807>": "d.maia",
    "SYSTEM | <@U01C19DCV50>": "d.regateiro",
    "SYSTEM | <@U019ELX89TP>": "d.gautam",
    "SYSTEM | <@UMSJT1XJ5>": "d.nunes",
    "SYSTEM | <@U1TB6NZ0Q>": "d-carlo.matteo",
    "SYSTEM | <@U9GGM46TB>": "p.harding",
    "SYSTEM | <@UUEKTT10A>": "u.yilmaz",
}


class CreateSupportIssueMessagePreview:
    def __init__(self):
        self.jira = jira_api.SKAJira()

    def preview(self, trigger_id: str, description: str, summary: str, user_id: str):
        users = []
        for po, jira_username in user_list.items():
            item = {
                "text": {"type": "plain_text", "text": "*" + po + "*"},
                "value": jira_username,
            }
            users.append(item)
            if user_id in po:
                initial_user = item

        result = {
            "trigger_id": trigger_id,
            "view": {
                "type": "modal",
                "callback_id": "view_create_support_issue",
                "title": {
                    "type": "plain_text",
                    "text": "JIRA Support Issue",
                },
                "submit": {"type": "plain_text", "text": "Submit"},
                "close": {"type": "plain_text", "text": "Cancel"},
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "mrkdwn",
                            "text": "This will create Jira Issue in the *Systems Team* "
                            "Jira project with the following:",
                        },
                    },
                    {"type": "divider"},
                    {
                        "type": "input",
                        "block_id": "block-issue-summary",
                        "element": {
                            "type": "plain_text_input",
                            "action_id": "action-issue-summary",
                            "max_length": 50,
                            "placeholder": {
                                "type": "plain_text",
                                "text": "Please enter a Issue Summary (Max 50 chars)",
                            },
                            "initial_value": summary,
                        },
                        "label": {
                            "type": "plain_text",
                            "text": "Issue Summary",
                        },
                    },
                    {
                        "type": "input",
                        "block_id": "block-issue-description",
                        "element": {
                            "type": "plain_text_input",
                            "action_id": "action-issue-description",
                            "initial_value": description,
                            "multiline": True,
                            "placeholder": {
                                "type": "plain_text",
                                "text": "This should be auto populated",
                            },
                        },
                        "label": {
                            "type": "plain_text",
                            "text": "Issue Description",
                        },
                    },
                    {
                        "type": "section",
                        "block_id": "block-issue-assignee",
                        "text": {
                            "type": "mrkdwn",
                            "text": "Pick JIRA User as Assignee",
                        },
                        "accessory": {
                            "action_id": "action-issue-assignee",
                            "type": "static_select",
                            "options": users,
                            "initial_option": initial_user,
                            "placeholder": {
                                "type": "plain_text",
                                "text": "Assign someone to the issue",
                            },
                        },
                    },
                ],
            },
        }
        return result

    def update(parameter_list):
        result = {
            "response_action": "update",
            "view": {
                "type": "modal",
                "title": {"type": "plain_text", "text": "Updated view"},
                "blocks": [
                    {
                        "type": "section",
                        "text": {
                            "type": "plain_text",
                            "text": "I've changed and I'll never be the same. "
                            "You must believe me.",
                        },
                    }
                ],
            },
        }
        return result
