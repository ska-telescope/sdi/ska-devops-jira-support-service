"""
This is a template web server for ska-devops related jobs
"""

from fastapi import FastAPI, Header, HTTPException

from app.routers import jira_event

app = FastAPI(root_path="/ska/devops/jira")
# app = FastAPI()


@app.get("/")
def test():
    return 200


async def get_token_header(x_token: str = Header(...)):
    """ Get token for security """
    if x_token != "fake-super-secret-token":
        raise HTTPException(status_code=400, detail="X-Token header invalid")


# app.include_router(
#     mrevent.router,
#     prefix="/events",
#     tags=["events"],
#     # dependencies=[Depends(get_token_header)],
#     responses={404: {"description": "Not found"}},
# )

app.include_router(
    jira_event.router,
    # prefix="/",
    tags=["jira", "support"],
    # dependencies=[Depends(get_token_header)],
    responses={404: {"description": "Not found"}},
)

# if __name__ == "__main__":
#     import uvicorn

#     uvicorn.run(app, host="0.0.0.0", port=3000)
